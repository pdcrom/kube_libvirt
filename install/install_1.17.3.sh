#!/bin/sh

init_master () {

priv_ip=$(ip -f inet -o addr show eth0|cut -d\  -f 7 | cut -d/ -f 1 | head -n 1)
sed -i "s/advertiseAddress.*/advertiseAddress: \"$priv_ip\"/" /tmp/install/kubeadm_master.yaml
/opt/bin/kubeadm init --config /tmp/install/kubeadm_master.yaml

mkdir -p /home/core/.kube
mkdir -p /root/.kube
cp -f /etc/kubernetes/admin.conf /root/.kube/config
cp -f /etc/kubernetes/admin.conf /home/core/.kube/config
chown core:core /home/core/.kube/config

export KUBECONFIG=/etc/kubernetes/admin.conf

kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
}

init_worker () {
  
chmod a+x /tmp/install/join-worker.sh
/tmp/install/join-worker.sh

}

## only run the installer on the firstboot
##
if [ -d "/opt/bin" ]; then
  # Control will enter here if $DIRECTORY exists.
  echo "installer will only run at first boot"
  exit 0
fi

PATH=$PATH:/opt/bin

systemctl enable docker && systemctl start docker

#CNI
CNI_VERSION="v0.8.2"
mkdir -p /opt/cni/bin
curl -Ls "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-linux-amd64-${CNI_VERSION}.tgz" | tar -C /opt/cni/bin -xz

#CRICTL
CRICTL_VERSION="v1.16.0"
mkdir -p /opt/bin
curl -Ls "https://github.com/kubernetes-sigs/cri-tools/releases/download/${CRICTL_VERSION}/crictl-${CRICTL_VERSION}-linux-amd64.tar.gz" | tar -C /opt/bin -xz

#KUBERNETES
RELEASE=v1.17.3
mkdir -p /opt/bin
cd /opt/bin
curl -Ls --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
chmod +x {kubeadm,kubelet,kubectl}
echo "Downloaded kubernetes $RELEASE"

curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/kubelet.service" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service
mkdir -p /etc/systemd/system/kubelet.service.d
curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/10-kubeadm.conf" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
systemctl enable kubelet && systemctl start kubelet
echo "Started kubelet"

#allow pull from private gitlab
mkdir -p /var/lib/kubelet
cp /tmp/install/docker_config.json /var/lib/kubelet/config.json

if [ $1 == 'master' ]
then
  init_master > /var/log/init_master.log
elif [ $1 == 'worker' ]
then
  echo ready  > /var/log/init_worker.log
else
exit 1
fi
