# disable update engine
data "ignition_systemd_unit" "update-engine" {
  name = "update-engine.service"
  mask = true
}

# disable locksmith
data "ignition_systemd_unit" "locksmithd" {
  name = "locksmithd.service"
  mask = true
}

# add local docker registry
data "ignition_systemd_unit" "docker" {
  name    = "docker.service"
  enabled = true
  dropin {
    name    = "50-insecure-registry.conf"
    content = "[Service]\nEnvironment='DOCKER_OPTS=--insecure-registry=dockerrepo:5000'"
  }
}

# set hostname
data "ignition_file" "hostname_worker" {
  count      = var.node_count
  # filesystem = "root" # default `ROOT` filesystem
  path       = "/etc/hostname"
  mode       = 420 # decimal 0644

  content {
    content = "${var.hostname_prefix}${var.node_type}${count.index}"
  }
}

# set ssh key
data "ignition_user" "core" {
  name = "core"

  ssh_authorized_keys = [
    file("${var.config.node_public_key_file}")
  ]
}

# data "ignition_networkd_unit" "network" {
#   count = var.node_count

#   name    = "00-eth0.network"
#   content = "[Match]\nName=eth0\n\n[Network]\nDNS=${var.config.network_dns}\nGateway=${var.config.network_gateway}\nAddress=${var.config.network_prefix}${var.network_ip_offset + count.index}/24"
# }

# build the ignition config
data "ignition_config" "ignition_worker" {
  count = var.node_count

  users = [
    data.ignition_user.core.rendered,
  ]

  # networkd = var.config.network_dhcp ? [] : ["${element(data.ignition_networkd_unit.network.*.rendered, count.index)}"]
  # networkd = [{\"units\":[{\"contents\":\"[Match]\\nName=eth0\\n\\n[Network]\\nDNS=10.8.8.2\\nGateway=10.8.8.2\\nAddress=10.8.8.20/24\",\"name\":\"00-eth0.network\"}]}"]

  files = [
    "${element(data.ignition_file.hostname_worker.*.rendered, count.index)}"
  ]

  systemd = [
    data.ignition_systemd_unit.update-engine.rendered,
    data.ignition_systemd_unit.locksmithd.rendered,
    data.ignition_systemd_unit.docker.rendered,
  ]
}

resource "libvirt_ignition" "ignition_worker" {
  count = var.node_count
  name  = "${var.hostname_prefix}${var.node_type}${count.index}.ign"
  pool  = var.config.volume_pool
  # content = data.ignition_config.ignition_worker.*.rendered[${count.index}]
  content = "${element(data.ignition_config.ignition_worker.*.rendered, count.index)}"
}
