resource "libvirt_volume" "node-disk" {
  count            = var.node_count
  name             = "${var.hostname_prefix}${var.node_type}${count.index}.qcow2"
  pool             = var.config.volume_pool
  base_volume_name = var.config.base_volume_name
  base_volume_pool = var.config.base_volume_pool
  size             = var.disk_size
}

resource "libvirt_domain" "node" {

  count  = var.node_count
  name   = "${var.hostname_prefix}${var.node_type}${count.index}"
  memory = var.memory
  vcpu   = var.vcpu

  coreos_ignition = libvirt_ignition.ignition_worker.*.id[count.index]
  fw_cfg_name     = "opt/org.flatcar-linux/config"

  network_interface {
    network_name   = var.config.network_name
    wait_for_lease = var.config.network_dhcp
  }

  disk {
    volume_id = libvirt_volume.node-disk.*.id[count.index]
  }

  graphics {
    type           = "vnc"
    listen_type    = "address"
    autoport       = true
    listen_address = "0.0.0.0"
  }

  connection {
    type        = "ssh"
    user        = "core"
    host        = var.config.network_dhcp ? "${self.network_interface.0.addresses.0}" : "${var.config.network_prefix}${var.network_ip_offset + count.index}"
    private_key = "${file(var.config.node_private_key_file)}"

    bastion_host        = var.config.bastion_host
    bastion_user        = var.config.bastion_user
    bastion_private_key = var.config.bastion_private_key_file != "" ? "${file(var.config.bastion_private_key_file)}" : ""
  }

  provisioner "file" {
    source      = var.config.install_folder
    destination = "/tmp/install"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /tmp/install/${var.config.install_script}",
      "sudo /tmp/install/${var.config.install_script} ${var.node_type}"
    ]
  }
}

output "ips" {

  #return the ip addresses of all nodes provisioned
  value = var.config.network_dhcp ? [for x in libvirt_domain.node : x.network_interface[0].addresses[0]] : [for x in range(var.node_count) : "${var.config.network_prefix}${var.network_ip_offset + x}"]
}

