# Pre-requisites

- working libvirt on server, e.g. on Centos 7
    https://www.cyberciti.biz/faq/how-to-install-kvm-on-centos-7-rhel-7-headless-server/

- terraform + libvirt plugin 

    https://github.com/dmacvicar/terraform-provider-libvirt
    https://gist.github.com/gwarf/3d63b24ff86a5311da87baa7ccbb7882

    centos 7:
```
        mkdir -p /root/.terraform.d/plugins
        cd /root/.terraform.d/plugins
        wget https://github.com/dmacvicar/terraform-provider-libvirt/releases/download/v0.6.1/terraform-provider-libvirt-0.6.1+git.1578064534.db13b678.Fedora_28.x86_64.tar.gz
        tar -xvzf terraform-provider-libvirt-0.6.1+git.1578064534.db13b678.Fedora_28.x86_64.tar.gz
        rm -rf terraform-provider-libvirt-0.6.1+git.1578064534.db13b678.Fedora_28.x86_64.tar.gz
```

- downloaded flatcar_production_qemu_image.img and put in libvirt volume_pool 
    https://docs.flatcar-linux.org/os/booting-with-libvirt/ 

- have bastion host ready and ssh-keys (optional)
    make sure ssh to bastion works from commandline: 
        ssh -i '/path/to/private-key' user@hostname

- prepare ssh-key for workers

# Configuration

copy config.tf.example to config.tf

The config.tf file contains most configuration variables, make sure to check/modify:
```
    #for libvirt_volume
    base_volume_name = "flatcar_production_qemu_image.img"
    base_volume_pool = "default"
    volume_pool      = "default"

    #for connection to node
    node_public_key_file  = "~/.ssh/id_rsa.pub"
    node_private_key_file = "~/.ssh/id_rsa"
```

The install/install<version>.sh file contains the version for the Kubernetes components:
```
#Install CNI plugins (required for most pod network):
CNI_VERSION="v0.8.2"

#Install crictl (required for kubeadm / Kubelet Container Runtime Interface (CRI))
CRICTL_VERSION="v1.17.0"

#Install kubeadm, kubelet, kubectl and add a kubelet systemd service:
RELEASE="v1.18.2"
```

The install/docker_config.json can be updated to allow kubernetes pull images from a private repo

# Installation

(tested with Terraform v0.12.24)
```
    terraform init
    terraform apply
```
When it completes it will list the master and worker ip addresses:

```
Outputs:

master_ips = [
  "10.80.80.223",
]
worker_ips = [
  "10.80.80.208",
  "10.80.80.65",
]
```

ssh into the master and list all nodes:

```
$ ssh core@10.80.80.223
Last login: Fri May  8 17:39:35 UTC 2020 from 10.80.80.1 on ssh
Flatcar Container Linux by Kinvolk stable (2303.4.0)
Update Strategy: No Reboots

$ kubectl get no -o wide
NAME               STATUS   ROLES    AGE    VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE                                                 KERNEL-VERSION    CONTAINER-RUNTIME
k8s-test-master0   Ready    master   108s   v1.18.2   10.80.80.223   <none>        Flatcar Container Linux by Kinvolk 2303.4.0 (Rhyolite)   4.19.95-flatcar   docker://18.6.3
k8s-test-worker0   Ready    <none>   74s    v1.18.2   10.80.80.208   <none>        Flatcar Container Linux by Kinvolk 2303.4.0 (Rhyolite)   4.19.95-flatcar   docker://18.6.3
k8s-test-worker1   Ready    <none>   80s    v1.18.2   10.80.80.65    <none>        Flatcar Container Linux by Kinvolk 2303.4.0 (Rhyolite)   4.19.95-flatcar   docker://18.6.3
```