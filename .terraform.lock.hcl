# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/community-terraform-providers/ignition" {
  version     = "2.1.2"
  constraints = "2.1.2"
  hashes = [
    "h1:gZYKv4Z7sWrIC2QOmuaCDp2XAUulYtp3TpJhpgnmgpU=",
    "zh:097a118ede40d9ba4824ffae2024e380455ad5252983f13942ccd6f33dc84e29",
    "zh:3b8f2c57950a8c8cb814da3a7c64b0ef350a3aac345ce2a87c86ecc80894a1f3",
    "zh:507f5317622a00961a2b7a5dc7a47eb8fa9c8d61ec65a60e5b84a6c1cee25fe3",
    "zh:85fccd324ba8655136343ca702aef179e696fce4bb4ea887255bbb109dc651e2",
    "zh:988218424ca391d2fbed89d8c13ece873260bc08cae201254aceda170738bcf3",
    "zh:996c5195caff0848a0dedf99e58ee1460fe9c9223ae95d837c30402ec55f43a8",
    "zh:9b34179f921dcadaf265795a8534bffc5546ee894a26c45f5c13246819bb0722",
    "zh:b8604e6fff5244314c154166037b7a637722c2919623731c2a09d7f874b89515",
    "zh:c76b068b89025f2a205e71b6025d1611d8d1f88d81af84106878a169f0f1c742",
    "zh:f19aafd5337515e01567979ac2f75e90baf092db92bd3a33e11d256f128ddd2a",
    "zh:f74c97517c380404a2abb2edaa38838a1d17a24cac978f817d5410fcea6085da",
  ]
}

provider "registry.terraform.io/hashicorp/libvirt" {
  version = "1.0.0"
  hashes = [
    "h1:yKo6c5J3LN9ms47cYYnr3mcqemSxfy2cOOc765jE9x0=",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.1.1"
  hashes = [
    "h1:Pctug/s/2Hg5FJqjYcTM0kPyx3AoYK1MpRWO0T9V2ns=",
    "zh:063466f41f1d9fd0dd93722840c1314f046d8760b1812fa67c34de0afcba5597",
    "zh:08c058e367de6debdad35fc24d97131c7cf75103baec8279aba3506a08b53faf",
    "zh:73ce6dff935150d6ddc6ac4a10071e02647d10175c173cfe5dca81f3d13d8afe",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:8fdd792a626413502e68c195f2097352bdc6a0df694f7df350ed784741eb587e",
    "zh:976bbaf268cb497400fd5b3c774d218f3933271864345f18deebe4dcbfcd6afa",
    "zh:b21b78ca581f98f4cdb7a366b03ae9db23a73dfa7df12c533d7c19b68e9e72e5",
    "zh:b7fc0c1615dbdb1d6fd4abb9c7dc7da286631f7ca2299fb9cd4664258ccfbff4",
    "zh:d1efc942b2c44345e0c29bc976594cb7278c38cfb8897b344669eafbc3cddf46",
    "zh:e356c245b3cd9d4789bab010893566acace682d7db877e52d40fc4ca34a50924",
    "zh:ea98802ba92fcfa8cf12cbce2e9e7ebe999afbf8ed47fa45fc847a098d89468b",
    "zh:eff8872458806499889f6927b5d954560f3d74bf20b6043409edf94d26cd906f",
  ]
}
