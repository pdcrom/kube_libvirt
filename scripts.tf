locals {

  set_ssh_flags = <<EOT
      unset ProxyCommand
      %{if local.config.bastion}
      ProxyCommand="-o ProxyCommand=\"ssh -W '%h:%p' -i ${local.config.bastion_private_key_file} ${local.config.bastion_user}@${local.config.bastion_host}\""
      %{endif}
      ssh_flags="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${local.config.node_private_key_file}"
  EOT  

}
